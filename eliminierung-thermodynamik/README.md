# Mechanistische Untersuchung einer Eliminierungsreaktion - Thermodynamische Aspekte

## Versuchsziele

  * Eliminierungsreaktion E1
  * Berechnung thermodynamischer Größen

## Aufgaben und Durchführung

In diesem Versuch soll die säurekatalysierte unimolekulare Dehydratisierung von 3,3-Dimetyl-2-Butanol nach dem E1-Mechanismus berechnet werden.
Die folgende Abbildung zeigt die zu untersuchende Reaktion [1]:

Experimentell wird beobachtet, dass sich hauptsächlich das thermodynamisch stabilere Buten bildet.
Berechnen Sie mit Hilfe quantenchemischer Methoden:

  * Welches ist das stabilere Carbokation
  * welches ist das stabilere Isomer

Das Berechnungsniveau sei B3LYP/6-31G*.
Warum sind hier keine diffusen Orbitale nötig?

## Auswertung

  * Bestimmen Sie die Reaktionsenthalpien ∆RH bzw. ∆RG. Welche Annahme können Sie für die Entropie ∆RS treffen?
    Begründen Sie.
  * Berechnen Sie unter Verwendung der bekannten Beziehung ∆RG = RT ln Keq die Gleichgewichtskonstante Keq und bestimmen Sie die prozentuale Zusammensetzung des Reaktionsgemischs.
    Experimentell wird für den Anteil des Hauptprodukts ein Wert von 84 % gefunden.
  * Stellen Sie die relevanten Molekülorbitale für einige ausgewählte Abstände des dissoziierenden Protons dar.
  * Anhand welcher Koordinaten (Bindungslänge, -winkel, Diederwinkel) könnte man beispielsweise den Reaktionsverlauf darstellen?