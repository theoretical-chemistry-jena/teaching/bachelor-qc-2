{
  description = ''

    Computational Chemistry Flake
    -----------------------------

    This flake provides packages and environments for computational chemistry,
    that is quantum chemistry, molecular mechanics, MD and some quantum
    dynamics. Use 
    
        nix flake show [ $PATH_TO_FLAKE_DIR ]

    to inspect the contents of the flake and

        nix flake metadate [ $PATH_TO_FLAKE_DIR ]

    to inspect version pins, last time of update and so on. Obtain an 
    interactive shell with packages available via
    
        nix shell $PATH_TO_FLAKE_DIR#$PACKAGE1 [ $PATH_TO_FLAKE_DIR#$PACKAGE2 ... ]
    
    e.g.

        nix shell ~/flakes/compchem#pyCompChem

    to obtain a python environment with common packages for common workflows.
    Alternatively directly run a programme via

        nix run $PATH_TO_FLAKE_DIR#PACKAGE -- $ARGUMENTS_TO_PROGRAMME
    
    e.g.
    
        nix run ~/flakes/compchem#xtb -- -ohess molecule.xyz
    
    to run xtb.
    
    Note the double dash to separate programme arguments from arguments to nix
    commands.
    Singularity containers for use on machines without Nix can be built using 
    the `singularityPkgs` package set, e.g.

        nix build .#singularityPkgs.molcas
        nix build .#singularityPkgs.pysisyphus
    
    to obtain singularity image files (.sif). Look for the `result` links. For
    singularity usage have a look at the excellent singularity tutorial at
    https://singularity-tutorial.github.io/.
  '';

  inputs = {
    qchem.url = "github:nix-qchem/nixos-qchem/master";

    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, qchem, flake-utils }: flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
    let
      pkgs = import qchem.inputs.nixpkgs {
        inherit system;
        overlays = [ qchem.overlays.default ];
        config = {
          allowUnfree = true;
          qchem-config = {
            # Target CPU generation for optimisations such as vectorisation.
            # Only available on x86, see https://gcc.gnu.org/onlinedocs/gcc/x86-Options.html
            # for a list of possible options 
            optArch = "haswell";

            # Access to some propietrary codes on the workgroup server. May be
            # used within the institute network but will produce errors outside.
            srcurl = "https://troja.ipc3.uni-jena.de/nix-src";

            # Enables CUDA support in some packages. This requires most often a
            # NixOS machine and certainly a recent NVidia GPU.
            useCuda = false;

            # Absolute path to a Q-Chem license file obtained via Mail. See also
            # https://github.com/markuskowa/NixOS-QChem#q-chem, e.g.
            # licQChem = "/home/phillip/.qchem.license.dat";

            allowEnv = false;
          };
        };
      };

      # A Python development environment for quantum chemistry.
      pyCompChem = pkgs.qchem.python3.withPackages (p: with p; [
        jupyterlab
        numpy
        scipy
        scikit-learn
        openmm
        gpaw
        pyscf
        ase
        pandas
        psi4
        pysisyphus
        xtb-python
        polyply
      ]);
    in
    {
      packages = {
        inherit (pkgs.qchem)
          # Visualisation and Analysis
          avogadro2 vmd gabedit wxmacmolplt iboview janpa luscus multiwfn
          pegamoid molden orient gdma travis-analyzer wfoverlap

          # System Setup
          moltemplate packmol pdbfixer polyply

          # Quantum Chemistry
          bagel cfour cp2k dalton dirac gamess-us macroqc mopac mrcc
          mrchem nwchem octopus molcas orca psi4 quantum-espresso siesta
          turbomole veloxchem xtb

          # Molecular Mechanics
          gromacs gromacsMpi openmm tinker

          # Optimisation and Dynamics
          crest i-pi pysisyphus
          ;

        # Packages directly from upstream.
        inherit (pkgs) avogadro lammps lammps-mpi povray tachyon;

        # Python environment for computational chemistry. Use via "nix shell"
        inherit pyCompChem;
      } // pkgs.lib.attrsets.optionalAttrs (pkgs.qchem.q-chem != null) {
        inherit (pkgs.qchem) q-chem q-chem-installer;
      };

      devShells.default = with pkgs; mkShell {
        buildInputs = [
          avogadro
          avogadro2
          pkgs.qchem.orca
          pkgs.qchem.gabedit
          pkgs.qchem.iboview
          pkgs.qchem.xtb
          pkgs.qchem.multiwfn
        ];
      };
    }
  );
}
