-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:      -40.4628315076
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                  4.9999985251 
   Number of Beta  Electrons                  4.9999985251 
   Total number of  Electrons                 9.9999970502 
   Exchange energy                           -6.5597460445 
   Correlation energy                        -0.3012518504 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy               -6.8609978949 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)     -40.4628315076 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 5
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 4
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.4198     6.0000    -0.4198     3.9298     3.9298    -0.0000
  1   0     0.8950     1.0000     0.1050     1.0029     1.0029    -0.0000
  2   0     0.8950     1.0000     0.1050     1.0029     1.0029     0.0000
  3   0     0.8950     1.0000     0.1050     1.0029     1.0029    -0.0000
  4   0     0.8951     1.0000     0.1049     1.0029     1.0029    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            1                0.982442
                0             6               2            1                0.982443
                0             6               3            1                0.982447
                0             6               4            1                0.982460
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:      -40.4644936418
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                  4.9999986022 
   Number of Beta  Electrons                  4.9999986022 
   Total number of  Electrons                 9.9999972043 
   Exchange energy                           -6.5269328969 
   Correlation energy                        -0.2998392358 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy               -6.8267721328 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)     -40.4644936418 
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:      -40.4645019829
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                  4.9999986087 
   Number of Beta  Electrons                  4.9999986087 
   Total number of  Electrons                 9.9999972174 
   Exchange energy                           -6.5245935940 
   Correlation energy                        -0.2997366699 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy               -6.8243302639 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)     -40.4645019829 
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:      -40.4645020225
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                  4.9999986092 
   Number of Beta  Electrons                  4.9999986092 
   Total number of  Electrons                 9.9999972184 
   Exchange energy                           -6.5244227679 
   Correlation energy                        -0.2997291035 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy               -6.8241518714 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)     -40.4645020225 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 4
   prop. index: 1
     Number of atoms                     : 5
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 4
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.4027     6.0000    -0.4027     3.9306     3.9306    -0.0000
  1   0     0.8993     1.0000     0.1007     1.0025     1.0025    -0.0000
  2   0     0.8993     1.0000     0.1007     1.0025     1.0025    -0.0000
  3   0     0.8993     1.0000     0.1007     1.0025     1.0025    -0.0000
  4   0     0.8994     1.0000     0.1006     1.0025     1.0025     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            1                0.982648
                0             6               2            1                0.982649
                0             6               3            1                0.982653
                0             6               4            1                0.982663
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 4
   prop. index: 1
       Filename                          : optfreq.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0000197883
        Electronic Contribution:
                  0    
      0      -0.000002
      1      -0.000000
      2       0.000008
        Nuclear Contribution:
                  0    
      0      -0.000002
      1       0.000006
      2      -0.000011
        Total Dipole moment:
                  0    
      0      -0.000004
      1       0.000006
      2      -0.000003
# -----------------------------------------------------------
$ Hessian
   description: Details about the Hessian
   geom. index: 4
   prop. index: 1
Normal modes:
Number of Rows: 15 Number of Columns: 15
                  0          1          2          3          4          5    
      0       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      1       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      2       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      3       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      4       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      5       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      6       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      7       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      8       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      9       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     10       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     11       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     12       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     13       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     14       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
                  6          7          8          9         10         11    
      0      -0.033259   0.031784   0.116392  -0.000007   0.000002   0.000041
      1       0.110627   0.056220   0.016259   0.000016   0.000002   0.000014
      2      -0.048155   0.107204  -0.043035  -0.000003  -0.000000   0.000004
      3      -0.023906   0.022850   0.083667  -0.000018   0.000015  -0.500383
      4      -0.534023  -0.271420  -0.078518  -0.499705  -0.018781   0.000009
      5       0.232466  -0.517550   0.207736  -0.018743   0.499672  -0.000005
      6       0.097948  -0.343099  -0.445121  -0.285192   0.375322   0.166663
      7      -0.432300   0.036896  -0.268430   0.267756   0.323781  -0.270941
      8       0.377518  -0.078569  -0.062645  -0.311314  -0.065304  -0.385721
      9       0.339526  -0.052801  -0.455295   0.467694   0.059351   0.166583
     10      -0.029722   0.012140   0.221858  -0.159141  -0.064795   0.469334
     11       0.187641  -0.542780   0.181082   0.077356  -0.492194  -0.041736
     12      -0.017261  -0.005674  -0.570143  -0.182405  -0.434714   0.166648
     13      -0.322152  -0.447521  -0.068643   0.390905  -0.240235  -0.198572
     14      -0.223822  -0.138504   0.186616   0.252734   0.057827   0.427413
                 12         13         14    
      0       0.078919   0.046866  -0.000621
      1      -0.045449   0.076818   0.021411
      2      -0.011451   0.018101  -0.089254
      3      -0.740870  -0.439858   0.005829
      4      -0.010304   0.017420   0.004850
      5      -0.002584   0.004112  -0.020186
      6      -0.173766   0.138685  -0.182997
      7       0.301266  -0.190779   0.302157
      8       0.440968  -0.292290   0.403061
      9       0.067254  -0.265939  -0.087821
     10       0.128773  -0.761672  -0.242162
     11      -0.014969   0.073372   0.001723
     12      -0.092989   0.008668   0.272385
     13       0.121821   0.019687  -0.319967
     14      -0.286963  -0.000876   0.678921
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 4
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :         16.0430000000
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :        -40.4645020225
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0000449607
        Number of frequencies          :     15      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6     1286.287344
      7     1286.376009
      8     1286.386935
      9     1509.533687
     10     1509.582853
     11     2973.550765
     12     3088.274541
     13     3088.340845
     14     3088.641801
        Zero Point Energy (Hartree)    :          0.0435516731
        Inner Energy (Hartree)         :        -40.4180728459
        Enthalpy (Hartree)             :        -40.4171286368
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0048232173
        Vibrational entropy            :          0.0000519855
        Translational entropy          :          0.0048232173
        Entropy                        :          0.0211548133
        Gibbs Energy (Hartree)         :        -40.4382834501
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     1 
    Coordinates:
               0 C     -2.698880000000    2.152240000000    0.000000000000
               1 H     -1.628880000000    2.152240000000    0.000000000000
               2 H     -3.055540000000    2.732050000000    0.825530000000
               3 H     -3.055550000000    1.147400000000    0.089370000000
               4 H     -3.055550000000    2.577250000000   -0.914900000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     2 
    Coordinates:
               0 C     -2.698879524624    2.152236391680    0.000001358138
               1 H     -1.605037580971    2.152235918313   -0.000003472552
               2 H     -3.063492661108    2.744975025583    0.843927739790
               3 H     -3.063493182531    1.125004320525    0.091364692523
               4 H     -3.063497050766    2.586728343899   -0.935290317899
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     3 
    Coordinates:
               0 C     -2.698879469283    2.152235093483    0.000002512850
               1 H     -1.603304745736    2.152234900558   -0.000005486047
               2 H     -3.064072343009    2.745915875063    0.845261760374
               3 H     -3.064069694017    1.123373811404    0.091510847012
               4 H     -3.064073747955    2.587420319492   -0.936769634189
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     4 
    Coordinates:
               0 C     -2.698879444005    2.152234207204    0.000003206557
               1 H     -1.603175503453    2.152234166984   -0.000006715760
               2 H     -3.064116764249    2.745986990736    0.845360029309
               3 H     -3.064111974289    1.123251329325    0.091522145455
               4 H     -3.064116314004    2.587473305750   -0.936878665561
