#! /usr/bin/env bash

FUNCTIONALS="vwn5 pbe pbe0 tpss tpss0 m062x wb97x-d3 b2ply-d3 wb97x-d4"
MOLECULES="ch4 ch3f1 ch2f2 ch1f3 cf4"

for f in $FUNCTIONALS; do
  for m in $MOLECULES; do
    rm -rf ../$f/$m
    mkdir -p ../$f/$m
    sed -e "s/{{ functional }}/$f/g" optfreq.inp > ../$f/$m/optfreq.inp
    cd ../$f/$m
    cp ../../templates/${m}.xyz geom.xyz
    sleep 1
    touch geom.xyz
    orca optfreq.inp | tee optfreq.out
    cd ../../templates
  done
done