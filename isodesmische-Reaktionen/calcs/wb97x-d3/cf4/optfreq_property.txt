-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -437.5670318259
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 20.9999911628 
   Number of Beta  Electrons                 20.9999911628 
   Total number of  Electrons                41.9999823256 
   Exchange energy                            0.0000000000 
   Correlation energy                         0.0000000000 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -33.6167589994 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -437.5670318259 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 5
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 4
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.4196     6.0000     0.5804     4.5750     4.5750     0.0000
  1   0     9.1451     9.0000    -0.1451     1.1636     1.1636     0.0000
  2   0     9.1451     9.0000    -0.1451     1.1636     1.1636    -0.0000
  3   0     9.1451     9.0000    -0.1451     1.1636     1.1636     0.0000
  4   0     9.1451     9.0000    -0.1451     1.1636     1.1636     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            9                1.143715
                0             6               2            9                1.143798
                0             6               3            9                1.143722
                0             6               4            9                1.143788
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0002209419
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -437.5815150569
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 20.9999919713 
   Number of Beta  Electrons                 20.9999919713 
   Total number of  Electrons                41.9999839426 
   Exchange energy                            0.0000000000 
   Correlation energy                         0.0000000000 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -33.7234065221 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -437.5815150569 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0001612969
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:     -437.5817126199
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 20.9999928162 
   Number of Beta  Electrons                 20.9999928162 
   Total number of  Electrons                41.9999856324 
   Exchange energy                            0.0000000000 
   Correlation energy                         0.0000000000 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -33.7410134740 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -437.5817126199 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0001534844
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:     -437.5817217993
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                 20.9999926630 
   Number of Beta  Electrons                 20.9999926630 
   Total number of  Electrons                41.9999853260 
   Exchange energy                            0.0000000000 
   Correlation energy                         0.0000000000 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -33.7376684911 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -437.5817217993 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0001549359
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 5
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:     -437.5817218374
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                 20.9999926663 
   Number of Beta  Electrons                 20.9999926663 
   Total number of  Electrons                41.9999853325 
   Exchange energy                            0.0000000000 
   Correlation energy                         0.0000000000 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -33.7377370968 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -437.5817218374 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 5
   prop. index: 1
     Number of atoms                     : 5
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 4
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     5.4085     6.0000     0.5915     4.6673     4.6673    -0.0000
  1   0     9.1479     9.0000    -0.1479     1.1622     1.1622    -0.0000
  2   0     9.1479     9.0000    -0.1479     1.1623     1.1623    -0.0000
  3   0     9.1480     9.0000    -0.1480     1.1621     1.1621    -0.0000
  4   0     9.1478     9.0000    -0.1478     1.1624     1.1624    -0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            9                1.166788
                0             6               2            9                1.166921
                0             6               3            9                1.166701
                0             6               4            9                1.166935
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0001549027
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 5
   prop. index: 1
       Filename                          : optfreq.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0010990731
        Electronic Contribution:
                  0    
      0      -0.000059
      1       0.000298
      2      -0.000210
        Nuclear Contribution:
                  0    
      0      -0.000017
      1       0.000020
      2      -0.000073
        Total Dipole moment:
                  0    
      0      -0.000077
      1       0.000318
      2      -0.000283
# -----------------------------------------------------------
$ Hessian
   description: Details about the Hessian
   geom. index: 5
   prop. index: 1
Normal modes:
Number of Rows: 15 Number of Columns: 15
                  0          1          2          3          4          5    
      0       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      1       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      2       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      3       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      4       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      5       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      6       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      7       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      8       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      9       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     10       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     11       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     12       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     13       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     14       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
                  6          7          8          9         10         11    
      0       0.000487   0.000640   0.192837  -0.283352  -0.037071  -0.000575
      1       0.000286   0.000008  -0.285711  -0.189082  -0.040996   0.000787
      2      -0.000472  -0.000330   0.012867   0.053588  -0.340609   0.000929
      3      -0.001011   0.002291   0.341393  -0.495468  -0.062588   0.499930
      4      -0.462364   0.191023   0.315842   0.212997   0.045918  -0.002092
      5       0.191469   0.462295  -0.015765  -0.057348   0.379115  -0.001000
      6      -0.100857   0.460907  -0.018542   0.285500   0.299385  -0.164975
      7       0.379501   0.179380  -0.004474   0.261724  -0.378498   0.270516
      8      -0.309020   0.070458  -0.475890   0.012487  -0.226309   0.386759
      9       0.448588  -0.143341  -0.411685   0.048473   0.018834  -0.168097
     10      -0.172123   0.010165  -0.230868  -0.534840  -0.016139  -0.469284
     11      -0.136926  -0.478823   0.033400   0.004444   0.384984   0.040390
     12      -0.347028  -0.320261  -0.033083   0.340636  -0.232194  -0.166494
     13       0.254805  -0.380574   0.100134   0.179661   0.374638   0.200362
     14       0.254776  -0.053721   0.450119   0.006537  -0.322449  -0.426737
                 12         13         14    
      0       0.538866  -0.740251  -0.118438
      1       0.724180   0.551664  -0.153148
      2      -0.193622   0.003394  -0.902639
      3      -0.187737   0.258999   0.040932
      4      -0.044519  -0.035129   0.009405
      5       0.012316  -0.000427   0.056044
      6      -0.027395   0.097970  -0.063082
      7      -0.055248  -0.119243   0.125067
      8      -0.002288  -0.121732   0.221586
      9      -0.118430   0.020327   0.017987
     10      -0.281283  -0.106416   0.038879
     11       0.032720   0.006043   0.053624
     12      -0.007123   0.090709   0.079042
     13      -0.076793  -0.087988  -0.076527
     14       0.079665   0.113970   0.239417
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 5
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :         88.0030000000
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :       -437.5818767401
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0010651521
        Number of frequencies          :     15      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6     434.858565
      7     435.446825
      8     633.349518
      9     633.730314
     10     633.798429
     11     923.641353
     12     1288.054642
     13     1288.443750
     14     1288.961224
        Zero Point Energy (Hartree)    :          0.0172235957
        Inner Energy (Hartree)         :       -437.5607554495
        Enthalpy (Hartree)             :       -437.5598112404
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0118546984
        Vibrational entropy            :          0.0014669208
        Translational entropy          :          0.0118546984
        Entropy                        :          0.0320118629
        Gibbs Energy (Hartree)         :       -437.5918231033
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     1 
    Coordinates:
               0 C     -2.698880000000    2.152230000000    0.000000000000
               1 F     -1.317370000000    2.147110000000   -0.002260000000
               2 F     -3.154870000000    2.899400000000    1.068840000000
               3 F     -3.164020000000    0.856230000000    0.112310000000
               4 F     -3.159260000000    2.706200000000   -1.178880000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     2 
    Coordinates:
               0 C     -2.698902052853    2.152258992254   -0.000047956392
               1 F     -1.372483016489    2.147599521430   -0.001937585702
               2 F     -3.136343549747    2.869476088289    1.026297480312
               3 F     -3.145713075481    0.907957744569    0.107708391441
               4 F     -3.140958305429    2.683877653456   -1.132010329659
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     3 
    Coordinates:
               0 C     -2.698901761417    2.152255610104   -0.000112298637
               1 F     -1.380857680213    2.147774135396   -0.001894203246
               2 F     -3.133492412072    2.864791487571    1.019810382470
               3 F     -3.143026061939    0.915833449303    0.107123116720
               4 F     -3.138122084359    2.680515317625   -1.124916997306
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     4 
    Coordinates:
               0 C     -2.698905675282    2.152263541351   -0.000110442184
               1 F     -1.379274597621    2.147775207951   -0.001922778545
               2 F     -3.134028546719    2.865632322833    1.021063237513
               3 F     -3.143568130864    0.914362094469    0.107261608561
               4 F     -3.138623049513    2.681136833397   -1.126281625345
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     5 
    Coordinates:
               0 C     -2.698907338289    2.152265752592   -0.000112876970
               1 F     -1.379310309486    2.147787113559   -0.001930341548
               2 F     -3.134015934607    2.865601811553    1.021043415976
               3 F     -3.143567759290    0.914398696700    0.107265092829
               4 F     -3.138598658328    2.681116625596   -1.126255290288
