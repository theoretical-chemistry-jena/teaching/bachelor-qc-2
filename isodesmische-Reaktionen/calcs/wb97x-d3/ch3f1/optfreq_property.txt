-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -139.7662593214
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                  9.0000045416 
   Number of Beta  Electrons                  9.0000045416 
   Total number of  Electrons                18.0000090831 
   Exchange energy                            0.0000000000 
   Correlation energy                         0.0000000000 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -11.8252903240 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -139.7662593214 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 5
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 4
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.0306     6.0000    -0.0306     3.9096     3.9096    -0.0000
  1   0     0.9079     1.0000     0.0921     0.9820     0.9820    -0.0000
  2   0     9.2455     9.0000    -0.2455     0.9775     0.9775     0.0000
  3   0     0.9078     1.0000     0.0922     0.9820     0.9820     0.0000
  4   0     0.9081     1.0000     0.0919     0.9820     0.9820     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            1                0.978091
                0             6               2            9                0.975407
                0             6               3            1                0.978068
                0             6               4            1                0.978076
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0001530121
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -139.7669465298
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                  9.0000040851 
   Number of Beta  Electrons                  9.0000040851 
   Total number of  Electrons                18.0000081703 
   Exchange energy                            0.0000000000 
   Correlation energy                         0.0000000000 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -11.8429665684 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -139.7669465298 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0001410683
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:     -139.7669546746
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                  9.0000040461 
   Number of Beta  Electrons                  9.0000040461 
   Total number of  Electrons                18.0000080922 
   Exchange energy                            0.0000000000 
   Correlation energy                         0.0000000000 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -11.8429931254 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -139.7669546746 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0001406883
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:     -139.7669586950
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                  9.0000040032 
   Number of Beta  Electrons                  9.0000040032 
   Total number of  Electrons                18.0000080063 
   Exchange energy                            0.0000000000 
   Correlation energy                         0.0000000000 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -11.8425456434 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -139.7669586950 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0001405032
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 5
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:     -139.7669586912
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                  9.0000040026 
   Number of Beta  Electrons                  9.0000040026 
   Total number of  Electrons                18.0000080053 
   Exchange energy                            0.0000000000 
   Correlation energy                         0.0000000000 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -11.8425201204 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -139.7669586912 
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 5
   prop. index: 1
     Number of atoms                     : 5
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 4
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     6.0406     6.0000    -0.0406     3.9109     3.9109     0.0000
  1   0     0.9035     1.0000     0.0965     0.9817     0.9817     0.0000
  2   0     9.2487     9.0000    -0.2487     0.9724     0.9724    -0.0000
  3   0     0.9034     1.0000     0.0966     0.9817     0.9817     0.0000
  4   0     0.9038     1.0000     0.0962     0.9817     0.9817     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0             6               1            1                0.979196
                0             6               2            9                0.973512
                0             6               3            1                0.979087
                0             6               4            1                0.979095
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0001405294
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 5
   prop. index: 1
       Filename                          : optfreq.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        1.8296949477
        Electronic Contribution:
                  0    
      0      -0.982594
      1       1.599531
      2       2.277506
        Nuclear Contribution:
                  0    
      0       1.222136
      1      -1.989565
      2      -2.833084
        Total Dipole moment:
                  0    
      0       0.239542
      1      -0.390033
      2      -0.555578
# -----------------------------------------------------------
$ Hessian
   description: Details about the Hessian
   geom. index: 5
   prop. index: 1
Normal modes:
Number of Rows: 15 Number of Columns: 15
                  0          1          2          3          4          5    
      0       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      1       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      2       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      3       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      4       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      5       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      6       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      7       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      8       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      9       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     10       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     11       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     12       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     13       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     14       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
                  6          7          8          9         10         11    
      0      -0.195729   0.069236  -0.106033  -0.030887   0.029460   0.051456
      1       0.320177   0.105337   0.038086   0.070878   0.032830  -0.003652
      2       0.455270  -0.045854  -0.072178   0.068816  -0.047239   0.032446
      3      -0.167058   0.075118  -0.116046  -0.013837   0.014113   0.024455
      4       0.202087  -0.420455   0.232034  -0.483999  -0.377150   0.382255
      5       0.285682  -0.186718   0.604241  -0.314526   0.602308   0.111809
      6       0.144591  -0.026860   0.041886  -0.005558   0.004034   0.006813
      7      -0.236077  -0.041516  -0.014839   0.010956   0.003833   0.000178
      8      -0.335762   0.018003   0.028235   0.011967  -0.007426   0.004152
      9      -0.115146  -0.322885   0.217594   0.375862   0.188727  -0.534981
     10       0.246789   0.314669  -0.054922  -0.152361  -0.053800   0.154203
     11       0.291075   0.610101   0.175063  -0.506394  -0.106449  -0.463088
     12      -0.110693  -0.070999   0.372473   0.110757  -0.629905  -0.231005
     13       0.185403  -0.366923  -0.351270  -0.414700  -0.032493  -0.496289
     14       0.326578  -0.216295  -0.451412  -0.224623   0.206978  -0.113597
                 12         13         14    
      0       0.014302  -0.054952  -0.071070
      1      -0.022696  -0.072897   0.033041
      2      -0.031896   0.027381  -0.054323
      3      -0.580833   0.495880   0.639189
      4      -0.008510  -0.006315   0.020864
      5      -0.011982   0.020918   0.006727
      6       0.000703   0.000272   0.000356
      7      -0.001139   0.000348  -0.000165
      8      -0.001608  -0.000142   0.000271
      9       0.199245   0.276653  -0.053819
     10       0.537832   0.751857  -0.092846
     11      -0.059928  -0.086509  -0.000095
     12       0.197917  -0.122859   0.254771
     13      -0.237405   0.116517  -0.318609
     14       0.482276  -0.257996   0.635552
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 5
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :         34.0330000000
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :       -139.7670992207
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0000745704
        Number of frequencies          :     15      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6     1085.554772
      7     1198.813992
      8     1204.990611
      9     1491.002288
     10     1500.250348
     11     1501.600331
     12     3055.316989
     13     3144.835375
     14     3145.082045
        Zero Point Energy (Hartree)    :          0.0394748283
        Inner Energy (Hartree)         :       -139.7247172791
        Enthalpy (Hartree)             :       -139.7237730700
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0088706512
        Vibrational entropy            :          0.0000874156
        Translational entropy          :          0.0088706512
        Entropy                        :          0.0263027956
        Gibbs Energy (Hartree)         :       -139.7500758657
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     1 
    Coordinates:
               0 C     -2.680740000000    2.122740000000   -0.041990000000
               1 H     -1.571340000000    2.122750000000   -0.041990000000
               2 F     -3.141240000000    2.871370000000    1.023880000000
               3 H     -3.050540000000    1.080900000000    0.050680000000
               4 H     -3.050540000000    2.563420000000   -0.990580000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     2 
    Coordinates:
               0 C     -2.678947940561    2.119871993714   -0.046147004649
               1 H     -1.588232609768    2.122545245306   -0.042322749449
               2 F     -3.137817106304    2.866271718178    1.016741679334
               3 H     -3.044732679078    1.096564170805    0.048423636716
               4 H     -3.044669664288    2.555926871997   -0.976695561951
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     3 
    Coordinates:
               0 C     -2.678403348109    2.119025598958   -0.047366225928
               1 H     -1.588078903994    2.123105228495   -0.041538286981
               2 F     -3.137514592550    2.865983387663    1.016367392409
               3 H     -3.045241866245    1.096654256049    0.048987113343
               4 H     -3.045161289102    2.556411528835   -0.976449992843
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     4 
    Coordinates:
               0 C     -2.677658479494    2.117892873611   -0.048991433467
               1 H     -1.587454261303    2.123859820026   -0.040487175609
               2 F     -3.137245035534    2.865777862265    1.016112738485
               3 H     -3.046074934334    1.096395595558    0.049807997057
               4 H     -3.045967289335    2.557253848541   -0.976442126465
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    5 
    Geometry Index:     5 
    Coordinates:
               0 C     -2.677654786172    2.117894822290   -0.048985307099
               1 H     -1.587453357612    2.123839611279   -0.040517624711
               2 F     -3.137276825557    2.865805398407    1.016148421943
               3 H     -3.046061621586    1.096395022632    0.049797278590
               4 H     -3.045953409073    2.557245145392   -0.976442768722
