# Ligandenfeldtheorie

## Versuchsziele

    * Ligandenfeldtheorie, Geometrie und Spin-Zustand (Multiplizität)
    * Verwendung von core potentials

## Aufgaben und Durchführung
Wie aus der Kristallfeldtheorie bekannt ist, können mehrere Faktoren die d-Elektronenkonguration eines Metallkomplexes beeinflussen.
Die wichtigsten Faktoren sind dabei das Metallatom selbst und die Stärke des Ligandenfelds [3, S. 637–982] [5, S. 459–490].
Für einen oktaedrischen d5-Metallkomplex gibt es zwei mögliche elektronische Konfigurationen, S = 5/2 und S = 1/2.
Berechnen Sie für einige ausgewählte d5-Übergangsmetallatome (Fe, 3d vs. Ru, 4d) und Liganden (CN– und acac– , acetylacetonat, starker vs. schwacher Ligand) mit Hilfe der Dichtefunktionaltheorie Strukturen der jeweiligen high-spin- und low-spin-Konfigurationen.

__Berechnungsniveau: DFT, (unrestricted) B3LYP und def2-SVP (def2/J) Basissatz +RIJCOSX__

  * Führen Sie eine Geometrieoptimierung für die jeweiligen Eisen- bzw. Rutheniumkomplexe Fe(acac)3, K3[Fe(CN)6] und Ru(acac)3 in der high- und low-spin-Elektronenkonfiguration durch.
    Für die Berechnung des high-spin-Zustandes von Ru(acac)3 geben Sie bitte eine Ru–O Bindungslänge von 2 Å an. 
    Zeigen Sie anhand einer Frequenzrechnung, dass Sie ein energetisches Minimum gefunden haben.
  * Vergleichen Sie die elektronischen Energien der Gasphase für die jeweiligen Spinkonfigurationen.
    In welcher Form liegen die Metallkomplexe vor? Stimmen Ihre berechneten Ergebnisse mit Ihren Erwartungen aus der Ligandenfeldtheorie überein?
  * Vergleichen Sie die berechneten Metall-Ligand-Bindungslängen mit den Werten in der Literatur [1, 2, 4, 6].
  * Berechnen Sie die magnetischen Suszeptibilitäten χ und die damit verknüpften effektiven magnetischen Momente μeff der Komplexe.
    In Übergangsmetallkomplexen ist μeff eng verwandt mit der Anzahl der ungepaarten Elektronen und kann somit helfen, den Spinzustand zu bestimmen.
    Da gepaarte Elektronen eines Moleküls schwach aus einem angelegten magnetischen Feld gestoßen werden, muss dieser Beitrag (χdia) von der gesamten (molaren) Suszeptibilität (χm) abgezogen werden.
    Damit erhält man die reine paramagnetische Suszeptibilität χpara.
    Das effektive magnetische Moment bei einer Temperatur T ist dann gleich (vgl. [4])
    
    (T in Kelvin).
    Unter der Annahme, dass nur ungepaarte Elektronen zum magnetischen Moment beitragen, kann man die folgende „reine Spin“-Beziehung aufstellen, um das magnetische Moment mit der Anzahl der ungepaarten Elektronen n zu verknüpfen (vgl. [4]):
    Verwenden Sie diese Gleichung, um die Anzahl der ungepaarten Elektronen in Ihrer Rechnung zu bestimmen.    

