# van-der-Waals-Wechselwirkungen zwischen Heliumatomen

## Versuchsziele

  * Benutzen verschiedener Korrelationsmethoden
  * Beschreiben der van-der-Waals-Wechselwirkungen mittels Lennard-Jonaes-Potenzialen

## Aufgaben

  * Berechnen Sie die Gesamtenergie des Heliumatoms
  * Berechnen Sie die Wechselwirkungsenergie des Heliumdimers.
  * Veranschaulichen Sie ihre Ergebnisse

## Durchführung

  1. Berechnen Sie die Gesamtenergie des Heliumatoms. Verwenden Sie dazu die Methoden HF, MP2, MP4, CISD, CCSD und QCISD(T).
     Mit den letzten beiden Methoden erhält man dieselbe Energie.
     Warum?
     Nutzen sie den `%basis`-Block und folgende Basissatzspezifikation:
     ```
     %basis
       newgto He
         S 4
         1 9079.303600 0.0006320
         2 58.789362 0.0414350
         3 10.200107 0.2992590
         4 2.346173 0.9749240
         S 1
         1 0.728426 1.0000000
         S 1
         1 0.283539 1.0000000
         S 1
         1 0.131665 1.0000000
         S 1
         1 0.060512 1.0000000
         P 1
         1 2.278600 1.0000000
         P 1
         1 0.539650 1.0000000
         P 1
         1 0.166860 1.0000000
         P 1
         1 0.050000 1.0000000
         D 1
         1 0.680000 1.0000000
         D 1
         1 0.180000 1.0000000
         D 1
         1 0.060000 1.0000000
       end
     end
     ```
  2. Berechnen Sie die Energie des Dimers, bei „unendlichem“ Bindungsabstand (z. B. R(He−He) 100 Å).
     Vergleichen Sie das Ergebnis mit der Energie zweier He-Atome.
     Bei einer der genannten Methoden gibt es hier Unterschiede.
     Bei welcher ist das der Fall und warum?
  3. Berechnen Sie die Wechselwirkungsenergie mit allen der obigen Methoden.
     Verwenden Sie z.B. folgende Abstände: R(He−He) = { 2.5, 3.0, 3.5 } Å.
  4. Fitten Sie Ihre Ergebnisse an ein Lennard-Jones-Potenzial.