-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:       -5.7175082113
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 2
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 0
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     2.0000     2.0000    -0.0000     0.0015     0.0015    -0.0000
  1   0     2.0000     2.0000     0.0000     0.0015     0.0015    -0.0000
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 1
   prop. index: 1
        Total number of Electrons                                                      4
        Number of correlated Electrons                                                 4
        Number of alpha correlated Electrons                                           2
        Number of beta correlated Electrons                                            2
        Reference Energy:                                                  -5.7175082113
        Total Correlation Energy:                                          -0.0762056900
        Alpha-Alpha Pairs Correlation Energy (No (T))                      -0.0000409805
        Beta-Beta Pairs Correlation Energy (No (T))                        -0.0000409805
        Alpha-Beta Pairs Correlation Energy (No (T))                       -0.0761237291
        Singlet pairs energy of double amplitudes (No (T))                 -0.0760833603
        Triplet pairs energy of double amplitudes (No (T))                 -0.0001229247
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000006117
        Triplet pairs energy of quadratic single amplitudes (No (T))       -0.0000000169
        Triples Correction Energy:                                          0.0000000000
        Total MDCI Energy:                                                 -5.7937139013
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 2
     Number of atoms                     : 2
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 0
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     2.0000     2.0000     0.0000     0.0635    -0.0011     0.0646
  1   0     2.0000     2.0000    -0.0000     0.0635    -0.0011     0.0646
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : he.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0000000000
        Electronic Contribution:
                  0    
      0      -0.000000
      1       0.000000
      2      -0.000000
        Nuclear Contribution:
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
        Total Dipole moment:
                  0    
      0      -0.000000
      1       0.000000
      2      -0.000000
# -----------------------------------------------------------
$ MDCI_Electric_Properties
   description: The MDCI Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : he.mdcip 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0000000001
        Electronic Contribution:
                  0    
      0      -0.000000
      1      -0.000000
      2       0.000000
        Nuclear Contribution:
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
        Total Dipole moment:
                  0    
      0      -0.000000
      1      -0.000000
      2       0.000000
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:       -5.7177287176
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 2
   prop. index: 1
        Total number of Electrons                                                      4
        Number of correlated Electrons                                                 4
        Number of alpha correlated Electrons                                           2
        Number of beta correlated Electrons                                            2
        Reference Energy:                                                  -5.7177287225
        Total Correlation Energy:                                          -0.0761040669
        Alpha-Alpha Pairs Correlation Energy (No (T))                      -0.0000138602
        Beta-Beta Pairs Correlation Energy (No (T))                        -0.0000138602
        Alpha-Beta Pairs Correlation Energy (No (T))                       -0.0760763284
        Singlet pairs energy of double amplitudes (No (T))                 -0.0760630569
        Triplet pairs energy of double amplitudes (No (T))                 -0.0000415771
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000005887
        Triplet pairs energy of quadratic single amplitudes (No (T))       -0.0000000036
        Triples Correction Energy:                                          0.0000000000
        Total MDCI Energy:                                                 -5.7938327895
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : he.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0000000000
        Electronic Contribution:
                  0    
      0      -0.000000
      1       0.000000
      2      -0.000000
        Nuclear Contribution:
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
        Total Dipole moment:
                  0    
      0      -0.000000
      1       0.000000
      2      -0.000000
# -----------------------------------------------------------
$ MDCI_Electric_Properties
   description: The MDCI Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : he.mdcip 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0000000001
        Electronic Contribution:
                  0    
      0      -0.000000
      1       0.000000
      2       0.000000
        Nuclear Contribution:
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
        Total Dipole moment:
                  0    
      0      -0.000000
      1       0.000000
      2       0.000000
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:       -5.7177510914
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 3
   prop. index: 1
        Total number of Electrons                                                      4
        Number of correlated Electrons                                                 4
        Number of alpha correlated Electrons                                           2
        Number of beta correlated Electrons                                            2
        Reference Energy:                                                  -5.7177511132
        Total Correlation Energy:                                          -0.0760623170
        Alpha-Alpha Pairs Correlation Energy (No (T))                      -0.0000052792
        Beta-Beta Pairs Correlation Energy (No (T))                        -0.0000052792
        Alpha-Beta Pairs Correlation Energy (No (T))                       -0.0760516776
        Singlet pairs energy of double amplitudes (No (T))                 -0.0760469797
        Triplet pairs energy of double amplitudes (No (T))                 -0.0000158370
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000005813
        Triplet pairs energy of quadratic single amplitudes (No (T))       -0.0000000006
        Triples Correction Energy:                                          0.0000000000
        Total MDCI Energy:                                                 -5.7938134302
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 3
   prop. index: 1
       Filename                          : he.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0000000000
        Electronic Contribution:
                  0    
      0      -0.000000
      1       0.000000
      2      -0.000000
        Nuclear Contribution:
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
        Total Dipole moment:
                  0    
      0      -0.000000
      1       0.000000
      2      -0.000000
# -----------------------------------------------------------
$ MDCI_Electric_Properties
   description: The MDCI Calculated Electric Properties
   geom. index: 3
   prop. index: 1
       Filename                          : he.mdcip 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0000000003
        Electronic Contribution:
                  0    
      0      -0.000000
      1      -0.000000
      2       0.000000
        Nuclear Contribution:
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
        Total Dipole moment:
                  0    
      0      -0.000000
      1      -0.000000
      2       0.000000
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:       -5.7177535090
# -----------------------------------------------------------
$ MDCI_Energies
   description: The MDCI energies
   geom. index: 4
   prop. index: 1
        Total number of Electrons                                                      4
        Number of correlated Electrons                                                 4
        Number of alpha correlated Electrons                                           2
        Number of beta correlated Electrons                                            2
        Reference Energy:                                                  -5.7177535124
        Total Correlation Energy:                                          -0.0760246471
        Alpha-Alpha Pairs Correlation Energy (No (T))                      -0.0000000000
        Beta-Beta Pairs Correlation Energy (No (T))                        -0.0000000000
        Alpha-Beta Pairs Correlation Energy (No (T))                       -0.0760246378
        Singlet pairs energy of double amplitudes (No (T))                 -0.0760252121
        Triplet pairs energy of double amplitudes (No (T))                 -0.0000000000
        Singlet pairs energy of quadratic single amplitudes (No (T))        0.0000005743
        Triplet pairs energy of quadratic single amplitudes (No (T))        0.0000000000
        Triples Correction Energy:                                          0.0000000000
        Total MDCI Energy:                                                 -5.7937781595
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 4
   prop. index: 1
       Filename                          : he.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0000000001
        Electronic Contribution:
                  0    
      0       0.000000
      1       0.000000
      2      -0.000000
        Nuclear Contribution:
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
        Total Dipole moment:
                  0    
      0       0.000000
      1       0.000000
      2      -0.000000
# -----------------------------------------------------------
$ MDCI_Electric_Properties
   description: The MDCI Calculated Electric Properties
   geom. index: 4
   prop. index: 1
       Filename                          : he.mdcip 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0000000000
        Electronic Contribution:
                  0    
      0      -0.000000
      1      -0.000000
      2      -0.000000
        Nuclear Contribution:
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
        Total Dipole moment:
                  0    
      0      -0.000000
      1      -0.000000
      2      -0.000000
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    2 
    Geometry Index:     1 
    Coordinates:
               0 He     0.000000000000    0.000000000000    0.000000000000
               1 He     2.500000000000    0.000000000000    0.000000000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    2 
    Geometry Index:     2 
    Coordinates:
               0 He     0.000000000000    0.000000000000    0.000000000000
               1 He     3.000000000000    0.000000000000    0.000000000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    2 
    Geometry Index:     3 
    Coordinates:
               0 He     0.000000000000    0.000000000000    0.000000000000
               1 He     3.500000000000    0.000000000000    0.000000000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    2 
    Geometry Index:     4 
    Coordinates:
               0 He     0.000000000000    0.000000000000    0.000000000000
               1 He   100.000000000000    0.000000000000    0.000000000000
