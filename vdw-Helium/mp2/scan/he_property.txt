-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:       -5.7175081985
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 2
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 0
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0     2.0000     2.0000    -0.0000     0.0015     0.0015    -0.0000
  1   0     2.0000     2.0000     0.0000     0.0015     0.0015    -0.0000
# -----------------------------------------------------------
$ MP2_Energies
   description: The MP2 energies
   geom. index: 1
   prop. index: 1
        Reference Energy:         -5.7175081985
        Correlation Energy:       -0.0639412536
        Total MP2 Energy:         -5.7814494521
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 1
   prop. index: 1
       Filename                          : he.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0000000000
        Electronic Contribution:
                  0    
      0      -0.000000
      1       0.000000
      2      -0.000000
        Nuclear Contribution:
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
        Total Dipole moment:
                  0    
      0      -0.000000
      1       0.000000
      2      -0.000000
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:       -5.7177287176
# -----------------------------------------------------------
$ MP2_Energies
   description: The MP2 energies
   geom. index: 2
   prop. index: 1
        Reference Energy:         -5.7177287176
        Correlation Energy:       -0.0638493391
        Total MP2 Energy:         -5.7815780566
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 2
   prop. index: 1
       Filename                          : he.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0000000000
        Electronic Contribution:
                  0    
      0      -0.000000
      1       0.000000
      2      -0.000000
        Nuclear Contribution:
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
        Total Dipole moment:
                  0    
      0      -0.000000
      1       0.000000
      2      -0.000000
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:       -5.7177510914
# -----------------------------------------------------------
$ MP2_Energies
   description: The MP2 energies
   geom. index: 3
   prop. index: 1
        Reference Energy:         -5.7177510914
        Correlation Energy:       -0.0638120163
        Total MP2 Energy:         -5.7815631077
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 3
   prop. index: 1
       Filename                          : he.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0000000000
        Electronic Contribution:
                  0    
      0      -0.000000
      1       0.000000
      2      -0.000000
        Nuclear Contribution:
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
        Total Dipole moment:
                  0    
      0      -0.000000
      1       0.000000
      2      -0.000000
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:       -5.7177535090
# -----------------------------------------------------------
$ MP2_Energies
   description: The MP2 energies
   geom. index: 4
   prop. index: 1
        Reference Energy:         -5.7177535090
        Correlation Energy:       -0.0637773534
        Total MP2 Energy:         -5.7815308625
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 4
   prop. index: 1
       Filename                          : he.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.0000000001
        Electronic Contribution:
                  0    
      0       0.000000
      1       0.000000
      2      -0.000000
        Nuclear Contribution:
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
        Total Dipole moment:
                  0    
      0       0.000000
      1       0.000000
      2      -0.000000
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    2 
    Geometry Index:     1 
    Coordinates:
               0 He     0.000000000000    0.000000000000    0.000000000000
               1 He     2.500000000000    0.000000000000    0.000000000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    2 
    Geometry Index:     2 
    Coordinates:
               0 He     0.000000000000    0.000000000000    0.000000000000
               1 He     3.000000000000    0.000000000000    0.000000000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    2 
    Geometry Index:     3 
    Coordinates:
               0 He     0.000000000000    0.000000000000    0.000000000000
               1 He     3.500000000000    0.000000000000    0.000000000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    2 
    Geometry Index:     4 
    Coordinates:
               0 He     0.000000000000    0.000000000000    0.000000000000
               1 He   100.000000000000    0.000000000000    0.000000000000
